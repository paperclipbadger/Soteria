module Main where

import Control.Monad ((<=<))
import System.Console.GetOpt
  ( getOpt
  , ArgOrder(RequireOrder)
  , OptDescr(Option)
  , ArgDescr(ReqArg, OptArg, NoArg)
  )
import System.Environment (getArgs)

import Syntax (parseModule)
import Pretty (renderModule)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [sourcefile] -> do
      contents <- readFile sourcefile
      case parseModule contents of
        Left err -> putStrLn err
        Right m ->
          putStrLn $ renderModule (internalize m)
    _ -> putStrLn "Expected exactly one argument."
