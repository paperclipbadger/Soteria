module Syntax
  ( Module(moduleName, exportsList, attributes, defintions)
  , Atom(..)
  , Name(..)
  , NameId(..)
  , NameId'(..)
  , Constant(..)
  , Literal(..)
  , ProgLoc(..)
  , ProgLoc'(..)
  , Annotation
  , Pattern(..)
  , Exp(..)
  , Clause(..)
  , parseModule
  , resolveProgLoc
  , resolveNameId
  , lookupAnnotation
  , exports
  , exportsAs
  ) where

import Data.List (find)
import Data.Map (Map(..), insert, insertWith, adjust, (!))
import qualified Data.Map as Map (empty, lookup)

import Control.Monad (liftM, foldM, mapM, mapM_, forM)
import Control.Monad.State (State(..), evalState, put, get, modify)
import Control.Monad.Writer (WriterT(..), runWriterT, tell, lift)

import qualified Language.CoreErlang.Syntax as CE


data Module = Module
  { moduleName    :: Atom
  , exportsList   :: [NameId]
  , attributes    :: [(Atom, Constant)]
  , definitions   :: [(NameId, ProgLoc)]
  , progStore     :: Map ProgLoc' Exp
  , annotationMap :: Map ProgLoc' Annotation
  , symbolMap     :: Map NameId' Name
  } deriving Show

newtype Atom = Atom { atomToString :: String } deriving Show

data Name
  = Function { name :: String, arity :: Integer }
  | Variable { name :: String }
  deriving (Show, Eq, Ord)

type NameId = (Atom, NameId')
newtype NameId' = NameId' Int deriving (Show, Eq, Ord)

allNameIds :: Atom -> [NameId]
allNameIds mn = [ (mn, NameId' i) | i <- [1..] ]

data Constant
  = CLit Literal
  | CTuple [Constant]
  | CList
    { cHead :: Constant
    , cTail :: Constant
    }
  deriving Show

data Literal
  = LChar Char
  | LString String
  | LInt Integer
  | LFloat Double
  | LAtom Atom
  | LNil
  deriving Show

-- Introduce an indirection via a store for easy program traversal
type ProgLoc = (Atom, ProgLoc')
newtype ProgLoc' = ProgLoc' Int deriving (Show, Eq, Ord)

allProgLocs :: Atom -> [ProgLoc]
allProgLocs mn = [ (mn, ProgLoc' i) | i <- [1..] ]

type Annotation = [Constant]

data Pattern
  = PVar NameId
  | PLit Literal
  | PTuple [Pattern]
  | PList Pattern Pattern
  | PBinary [(Pattern, [ProgLoc])]
  | PAlias NameId Pattern
  deriving Show

data Exp
  = EValueList [NameId]
  | ELit Literal
  | EName NameId
  | ETuple [NameId]
  | EList NameId NameId
  | EBinary [(NameId, [NameId])]
  | ELet
    { eNames       :: [NameId]
    , eHead        :: ProgLoc
    , eBody        :: ProgLoc
    }
  | ECase
    { eHead        :: ProgLoc
    , eClauses     :: [Clause]
    }
  | EFun
    { eNames       :: [NameId]
    , eBody        :: ProgLoc
    }
  | ELetrec
    { eDefs        :: [(NameId, ProgLoc)]
    , eBody        :: ProgLoc
    }
  | EApply
    { eFun         :: ProgLoc
    , eArgs        :: [ProgLoc]
    }
  | EInterCall
    { eModuleName  :: ProgLoc
    , eFunName     :: ProgLoc
    , eArgs        :: [ProgLoc]
    }
  | EPrimOpCall
    { eOpName      :: Atom
    , eArgs        :: [ProgLoc]
    }
  | ETry
    { eHead        :: ProgLoc
    , eSuccNames   :: [NameId]
    , eSuccBody    :: ProgLoc
    , eFailNames   :: [NameId]
    , eFailBody    :: ProgLoc
    }
  | EReceive
    { eClauses     :: [Clause]
    , eTimeout     :: ProgLoc
    , eTimeoutBody :: ProgLoc
    }
  | EDo
    { eHead        :: ProgLoc
    , eBody        :: ProgLoc
    }
  | ECatch     { eBody        :: ProgLoc }
  deriving Show

data Clause
  = Clause
  { cPatterns :: [Pattern]
  , cWhere    :: ProgLoc
  , cBody     :: ProgLoc
  } deriving Show

--------------

-- | Parses a CoreErlang module. On success, returns @Right m@ where @m@ is the
-- 'Module' corresponding to the input string. On failure, returns @Left s@
-- where @s@ is the error message.
parseModule :: String -> Either String Module
parseModule s
  = case (CE.parseModule m) of
    Left e  -> Left $ show e
    Right m -> Right $ internalize m

resolveProgLoc :: Module -> ProgLoc -> Exp
resolveProgLoc m (mn, p)
  | moduleName m == mn = (progStore m) ! p
  | otherwise = error "ProgLoc is from a different module"

resolveNameId :: Module -> NameId -> Name
resolveNameId m (mn, nid)
  | moduleName m == mn = (symbolMap m) ! nid
  | otherwise = error "NameId is from a different module"

lookupAnnotation :: Module -> ProgLoc -> Maybe Annotation
lookupAnnotation m (mn, p)
  | moduleName m == mn = Map.lookup p (annotationMap m)
  | otherwise = error "ProgLoc is from a different module"

exports :: Module -> Name -> Bool
exports m n = exportsAs m n /= Nothing

exportsAs :: Module -> Name -> Maybe NameId
exportsAs m n = find (\(_, nid) -> (symbolMap ! nid) == n) $ exportsList m

--------------

data InternalizeState
  = InternalizeState
    { iProgStore     :: Map ProgLoc' Exp
    , iAnnotationMap :: Map ProgLoc' Annotation
    , iSymbolMap     :: Map NameId' Name
    , scopingTable   :: Map Name [NameId]
    , freshProgLocs  :: [ProgLoc]
    , freshNameIds   :: [NameId]
    }

type Internalizing = State InternalizeState

freshProgLoc :: Internalizing ProgLoc
freshProgLoc = do
  s <- get
  let (p:ps) = freshProgLocs s
  put s{freshProgLocs = ps}
  return p

freshNameId :: Internalizing NameId
freshNameId = do
  s <- get
  let (n:ns) = freshNameIds s
  put s{freshNameIds = ns}
  return n

addToStore :: Exp -> Internalizing ProgLoc
addToStore e = do
  l@(_, l') <- freshProgLoc
  modify $ \s -> s{iProgStore = insert l' e (iProgStore s)}
  return l

addAnnotation :: ProgLoc -> [Constant] -> Internalizing ()
addAnnotation (_, l) cs
  = modify $ \s -> s{iAnnotationMap = insert l cs (iAnnotationMap s)}

pushScope :: Name -> Internalizing NameId
pushScope n = do
  nid@(_, nid') <- freshNameId
  modify $ \s -> s
    { iSymbolMap = insert nid' n (iSymbolMap s)
    , scopingTable = insertWith (++) n [nid] (scopingTable s)
    }
  return nid

pushScopes :: [Name] -> Internalizing [NameId]
pushScopes = mapM pushScope

popScope :: Name -> Internalizing ()
popScope n = modify $ \s -> s{scopingTable = adjust (drop 1) n (scopingTable s)}

popScopes :: [Name] -> Internalizing ()
popScopes ns = mapM_ popScope ns

getNameId :: Name -> Internalizing (Maybe NameId)
getNameId n = do
  s <- get
  case Map.lookup n (scopingTable s) of
    Nothing    -> return Nothing
    Just []    -> return Nothing
    Just (nid:_) -> return $ Just nid

-- | Creates a fresh 'NameId' which maps to @'Variable' \"SoteriaInternal\"@.
-- Useful when, for instance, rewriting a tuple @{\'a\', (fun () -> \'b\')()}@
-- as @(fun (V0, V1) -> {V0, V1})(\'a\', (fun () -> \'b\')())@ with @V0@ and
-- @V1@ fresh.
freshVariable :: Internalizing NameId
freshVariable = pushScope (Variable "SoteriaInternal")

-- | Converts a 'Language.CoreErlang.Syntax.Module' to an internal
-- representation (a 'Module'). Modifications include:
--
-- * Replace sub-expressions with 'ProgLoc's.
-- Use 'resolveProgLoc' to get the sub-expression back.
--
-- * Replace names (function names and variables) with 'NameId's.
-- Use 'resolveNameId' to get the name back.
-- Every time a name is bound, it is assigned a fresh 'NameId'. Occurences of
-- a bound name are replaced with the 'NameId' of the most recent binding.
-- Hence
--
--     > let <X> = 'a' in let <X> = 'b' in X
--
--     becomes
--
--     > let <nid0> = 'a' in let <nid1> = 'b' in nid1
--
--     where @nid0@ and @nid1@ are distinct 'NameId's and both resolve to
--     @'Variable' \"X\"@.
--
-- * Rewrite list constuctors to a canonical form. @[e0, e1 | e2]@ is rewritten
-- as @[e0 | [e1 | e2]]@ and @[e0, e1]@ is rewritten @[e0 | [e1 | []]]@.
--
-- * Replace constructor applications with lambda applications. For instance,
-- @{e0, e1, e2}@ becomes @(fun (n0, n1, n2) -> {n0, n1, n2})(e0, e1)@
-- and @[e0|e1]@ becomes @(fun (n0, n1) -> [n0|n1])(e0, e1)@, with in each case
-- @n0@, @n1@ and @n2@ distinct, fresh 'NameId's. The constructors that we
-- do this for are:
--
--     * List @[e0 | e1]@
--     * Tuple @{e0, ..., eN}@
--     * Binary @#{\<e00>(e01, ..., e0N), ..., \<eM0>(eM1, ..., eML)}#@
--     * ValueList @\<e1, ..., eN>@
--
--     We do this so that we can represent fully evaluated constructors as
--     closures, using 'ProgLoc's instead of expressions:
--
--     @
--         type Closure = Closure 'ProgLoc' ('Data.Map' 'NameId' Closure)
--     @
internalize :: CE.Ann CE.Module -> Module
internalize m
  = evalState (iModule m') $
    InternalizeState
      { iProgStore     = Map.empty
      , iAnnotationMap = Map.empty
      , iSymbolMap     = Map.empty
      , scopingTable   = Map.empty
      , freshProgLocs  = allProgLocs (Atom n)
      , freshNameIds   = allNameIds (Atom n)
      }
  where m'@(CE.Module (CE.Atom n) _ _ _) = stripAnn m

stripAnn :: CE.Ann a -> a
stripAnn (CE.Constr a) = a
stripAnn (CE.Ann a _) = a

iModule :: CE.Module -> Internalizing Module
iModule (CE.Module (CE.Atom n) es as defs) = do
  as' <- mapM iAttribute as
  pushScopes names
  es' <- mapM iFunName es
  defs' <- mapM iFunDef defs
  popScopes names
  s <- get
  return $
    Module
      { moduleName    = Atom n
      , exportsList   = es'
      , attributes    = as'
      , definitions   = defs'
      , progStore     = iProgStore s
      , annotationMap = iAnnotationMap s
      , symbolMap     = iSymbolMap s
      }
  where
    iAttribute :: (CE.Atom, CE.Const) -> Internalizing (Atom, Constant)
    iAttribute (CE.Atom a, c) = do
      c' <- iConst c
      return (Atom a, c')

    names = map funDefName defs

funDefName :: CE.FunDef -> Name
funDefName (CE.FunDef an _) = funName . stripAnn $ an

funName :: CE.Function -> Name
funName (CE.Function ((CE.Atom f), i)) = Function f i

iFunDef :: CE.FunDef -> Internalizing (NameId, ProgLoc)
iFunDef (CE.FunDef af ae) = do
  nid <- iFunName (stripAnn af)
  p <- iAnnExp ae
  return (nid, p)

iFunName :: CE.Function -> Internalizing NameId
iFunName f = do
  (Just nid) <- getNameId . funName $ f
  return nid

iConst :: CE.Const -> Internalizing Constant
iConst (CE.CLit l) = CLit <$> iLiteral l
iConst (CE.CTuple cs) = CTuple <$> mapM iConst cs
iConst (CE.CList l) = iConstList l
  where
    iConstList :: CE.List CE.Const -> Internalizing Constant
    iConstList (CE.L cs) = do
      cs' <- mapM iConst cs
      return $ foldr CList (CLit LNil) cs'
    iConstList (CE.LL cs c) = do
      cs' <- mapM iConst cs
      c' <- iConst c
      return $ foldr CList c' cs'

iLiteral :: CE.Literal -> Internalizing Literal
iLiteral (CE.LChar c) = return $ LChar c
iLiteral (CE.LString s) = return $ LString s
iLiteral (CE.LInt i) = return $ LInt i
iLiteral (CE.LFloat f) = return $ LFloat f
iLiteral (CE.LAtom (CE.Atom a)) = return $ LAtom (Atom a)
iLiteral CE.LNil = return $ LNil

iExps :: CE.Exps -> Internalizing ProgLoc
iExps (CE.Exp ae) = iAnnExp ae
iExps (CE.Exps (CE.Constr es)) = do
  ps <- mapM iAnnExp es
  exp <- constructorLambda (\ns -> EValueList ns) ps
  addToStore exp
iExps (CE.Exps (CE.Ann es cs)) = do
  ps <- mapM iAnnExp es
  exp <- constructorLambda (\ns -> EValueList ns) ps
  p <- addToStore exp
  cs' <- mapM iConst cs
  addAnnotation p cs'
  return p

-- | Takes @cons@ and @[p0, ..., pN]@ and returns a 'ProgLoc' that resolves to
-- @(fun (n1, ..., nN) -> cons [n1, ..., nN])(p1, ..., pN)@
constructorLambda :: ([NameId] -> Exp) -> [ProgLoc] -> Internalizing Exp
constructorLambda constructor arglocs = do
  names <- sequence $ replicate (length arglocs) freshVariable
  consloc <- addToStore $ constructor names
  funcloc <- addToStore $ EFun names consloc
  return $ EApply funcloc arglocs

iAnnExp :: CE.Ann CE.Exp -> Internalizing ProgLoc
iAnnExp (CE.Constr a) = iExp a
iAnnExp (CE.Ann a cs) = do
   p <- iExp a
   cs' <- mapM iConst cs
   addAnnotation p cs'
   return p

iExp :: CE.Exp -> Internalizing ProgLoc
iExp e = iExp' e >>= addToStore

iExp' :: CE.Exp -> Internalizing Exp
iExp' (CE.Lit l) = ELit <$> iLiteral l
iExp' (CE.Var v) = EName <$> iVarName v
iExp' (CE.Fun f) = EName <$> iFunName f
iExp' (CE.App e es) = EApply <$> iExps e <*> mapM iExps es
iExp' (CE.ModCall (m, f) es)
  = EInterCall <$> iExps m <*> iExps f <*> mapM iExps es
iExp' (CE.Lambda vs e) = do
  let names = map varName vs
  pushScopes names
  vs' <- mapM iVarName vs
  p <- iExps e
  popScopes names
  return $ EFun vs' p
iExp' (CE.Seq e1 e2) = EDo <$> iExps e1 <*> iExps e2
iExp' (CE.Let (vs, e) e') = do
  p <- iExps e
  let names = map varName vs
  pushScopes names
  vs' <- mapM iVarName vs
  p' <- iExps e'
  popScopes names
  return $ ELet vs' p p'
iExp' (CE.LetRec defs e) = do
  let names = map funDefName defs
  pushScopes names
  defs' <- mapM iFunDef defs
  p <- iExps e
  popScopes names
  return $ ELetrec defs' p
iExp' (CE.Case e alts) = ECase <$> iExps e <*> mapM (iAlt . stripAnn) alts
iExp' (CE.Tuple es) = do
  ps <- mapM iExps es
  constructorLambda (\ns -> ETuple ns) ps
iExp' (CE.List l) = iExpsList l
  where
    iExpsList :: CE.List CE.Exps -> Internalizing Exp
    iExpsList (CE.L es) = do
      es' <- mapM iExps es
      foldM foldf (ELit LNil) . reverse $ es'
    iExpsList (CE.LL es e) = do
      es' <- mapM iExps es
      p <- iExps e
      let (last:rest) = reverse es'  --TODO: Write a version that doesn't break on es = []
      foldM foldf (EList last p) rest

    foldf :: Exp -> ProgLoc -> Internalizing Exp
    foldf t h = do
      p <- addToStore t
      constructorLambda (\[hn, tn] -> EList hn tn) [h, p]
iExp' (CE.Binary bs) = do
  -- Even more horrible than it should be because we rewrite the constructor
  -- as a lambda application.
  rs <- mapM f bs
  let
    (replacementss, bs') = unzip rs
    (names, arglocs) = unzip (concat replacementss)
  consloc <- addToStore $ EBinary bs
  funcloc <- addToStore $ EFun names consloc
  return $ EApply funcloc arglocs
  where
    f :: CE.BitString CE.Exps -> Internalizing ([(NameId, Progloc)], (NameId, [NameId]))
    f (CE.BitString e es) = do
      argloc <- iExps e
      name <- freshVariable
      arglocs <- mapM iExps es
      names <- sequence (replicate (length es) freshVariable)
      return ((name, argloc) : zip names arglocs, (name, names))
iExp' (CE.Op (CE.Atom a) es) = EPrimOpCall (Atom a) <$> mapM iExps es
iExp' (CE.Try e0 (vs1, e1) (vs2, e2)) = do
  let
    names1 = map varName vs1
    names2 = map varName vs2
  p0 <- iExps e0
  pushScopes names1
  vs1' <- mapM iVarName vs1
  p1 <- iExps e1
  popScopes names1
  pushScopes names2
  vs2' <- mapM iVarName vs2
  p2 <- iExps e2
  popScopes names2
  return $ ETry p0 vs1' p1 vs2' p2
iExp' (CE.Rec alts (CE.TimeOut e e'))
  = EReceive <$> mapM (iAlt . stripAnn) alts <*> iExps e <*> iExps e'
iExp' (CE.Catch e) = ECatch <$> iExps e

iVarName :: CE.Var -> Internalizing NameId
iVarName s = do
  (Just nid) <- getNameId (varName s)
  return nid

varName :: CE.Var -> Name
varName = Variable

iAlt :: CE.Alt -> Internalizing Clause
iAlt (CE.Alt ps (CE.Guard g) e) = do
  (ps', boundnames) <- iPats ps
  gp <- iExps g
  ep <- iExps e
  popScopes boundnames
  return $ Clause ps' gp ep

iPats :: CE.Pats -> Internalizing ([Pattern], [Name])
iPats (CE.Pat p) = do
  (p, ns) <- runWriterT $ iPat p
  return ([p], ns)
iPats (CE.Pats ps) = runWriterT $ mapM iPat ps

iPat :: CE.Pat -> WriterT [Name] Internalizing Pattern
iPat (CE.PVar v) = do
  let n = varName v
  tell [n]
  lift $ pushScope n
  v' <- lift $ iVarName v
  return $ PVar v'
iPat (CE.PLit l) = PLit <$> lift (iLiteral l)
iPat (CE.PTuple ps) = PTuple <$> mapM iPat ps
iPat (CE.PList l)
  = iPatList l
  where
    iPatList :: CE.List CE.Pat -> WriterT [Name] Internalizing Pattern
    iPatList (CE.L ps) = do
      ps' <- mapM iPat ps
      foldM foldf (PLit LNil) . reverse $ ps'
    iPatList (CE.LL ps p) = do
      ps' <- mapM iPat ps
      p' <- iPat p
      foldM foldf p' . reverse $ ps'

    foldf :: Pattern -> Pattern -> WriterT [Name] Internalizing Pattern
    foldf t h = return $ PList h t
iPat (CE.PBinary bs)
  = PBinary <$> mapM f bs
  where f (CE.BitString p es) = (,) <$> iPat p <*> lift (mapM iExps es)
iPat (CE.PAlias (CE.Alias v p)) = do
  let n = varName v
  tell [n]
  lift $ pushScope n
  v' <- lift $ iVarName v
  p' <- iPat p
  return $ PAlias v' p'
