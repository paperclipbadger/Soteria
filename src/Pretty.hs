module Pretty (renderModule) where

import Text.PrettyPrint ((<>), (<+>), ($$), ($+$))
import qualified Text.PrettyPrint as PP

import Syntax

renderModule :: Module -> String
renderModule
  = PP.renderStyle PP.style{PP.lineLength = 80} . prettyModule

docseq :: [PP.Doc] -> PP.Doc
docseq = PP.sep . PP.punctuate PP.comma

list :: [PP.Doc] -> PP.Doc
list = PP.brackets . docseq

tuple :: [PP.Doc] -> PP.Doc
tuple = PP.braces . docseq

args :: [PP.Doc] -> PP.Doc
args = PP.parens . docseq

angles :: PP.Doc -> PP.Doc
angles d = PP.char '<' <> d <> PP.char '>'

indent :: PP.Doc -> PP.Doc
indent = PP.nest 4

prettyModule :: Module -> PP.Doc
prettyModule m
  = PP.sep [PP.text "module" <+> name, indent exps, indent attr] $$ defs $$ PP.text "end"
  where
    prettyAttr :: (Atom, Constant) -> PP.Doc
    prettyAttr (a, c) = prettyAtom a <+> PP.equals <+> prettyConstant c

    name = prettyAtom $ moduleName m
    exps = list . map (prettyNameId m) $ exportsList m
    attr = PP.text "attributes" <+> (list . map prettyAttr $ attributes m)
    defs = PP.cat . map (prettyDef m) $ definitions m

prettyAtom :: Atom -> PP.Doc
prettyAtom = PP.quotes . PP.text . atomToString

prettyConstant :: Constant -> PP.Doc
prettyConstant (CLit l) = prettyLiteral l
prettyConstant (CTuple cs) = tuple . map prettyConstant $ cs
prettyConstant (CList h t)
  = PP.cat [PP.lbrack <> prettyConstant h, PP.char '|' <> prettyConstant t <> PP.rbrack]

prettyLiteral :: Literal -> PP.Doc
prettyLiteral (LChar c) = PP.quotes (PP.char c)
prettyLiteral (LString s) = PP.doubleQuotes (PP.text s)
prettyLiteral (LInt i) = PP.integer i
prettyLiteral (LFloat d) = PP.double d
prettyLiteral (LAtom a) = prettyAtom a
prettyLiteral (LNil) = PP.text "[]"

prettyDef :: Module -> (NameId, ProgLoc) -> PP.Doc
prettyDef m (n, p)
  = PP.sep [prettyNameId m n <+> PP.equals, indent $ prettyProgLoc m p]

prettyNameId :: Module -> NameId -> PP.Doc
prettyNameId m nid@(_, NameId' i)
  = name <> PP.text "_" <> PP.text (show i)
  where name = prettyName (resolveNameId m nid)

prettyName :: Name -> PP.Doc
prettyName (Function n i) = PP.quotes (PP.text n) <> PP.char '/' <> PP.integer i
prettyName (Variable n) = PP.text n

prettyProgLoc :: Module -> ProgLoc -> PP.Doc
prettyProgLoc m p@(_, ProgLoc' i)
  = PP.cat [PP.text (show i) <> PP.text ":", annexp]
  where
    pexp = prettyExp m (resolveProgLoc m p)
    annexp = case lookupAnnotation m p of
      (Just cs) ->
        let pcs = list . map prettyConstant $ cs
        in PP.parens $ PP.sep [pexp, PP.text "-|" <+> pcs]
      Nothing   -> pexp

prettyExp :: Module -> Exp -> PP.Doc
prettyExp m (EValueList ns) = angles . docseq . map (prettyNameId m) $ ns
prettyExp m (ELit l) = prettyLiteral l
prettyExp m (EName nid) = prettyNameId m nid
prettyExp m (ETuple ns) = tuple $ prettyNameId m ns
prettyExp m (EList h t)
  = PP.sep [PP.lbrack <+> prettyNameId m h, prettyNameId m t]
prettyExp m (EBinary bs)
  = PP.text "#{" <> PP.sep (PP.punctuate PP.comma . map prettyBitstring $ bs) <> PP.text "}#"
  where
    bitstrings = PP.sep . PP.punctuate PP.comma . map prettyBitstring $ bs
    prettyBitstring :: (NameId, [NameId]) -> PP.Doc
    prettyBitstring (n, ns)
      = PP.char '#' <> angles (prettyNameId m n) <> exps
      where exps = args $ map (prettyNameId m) ns
prettyExp m (ELet vs a e)
  = PP.sep [head, indent e']
  where
    head = PP.sep [PP.text "let", indent vars, PP.equals, indent a', PP.text "in"]
    vars = angles . docseq . map (prettyNameId m) $ vs
    a' = (prettyProgLoc m a)
    e' = (prettyProgLoc m e)
prettyExp m (ECase p cs)
  = PP.vcat [PP.sep [PP.text "case", indent exp, PP.text "of"], indent clauses, PP.text "end"]
  where
    exp = prettyProgLoc m p
    clauses = PP.vcat . map (prettyClause m) $ cs
prettyExp m (EFun vs b)
  = PP.sep [head, indent body]
  where
    head = PP.text "fun" <+> (args . map (prettyNameId m) $ vs) <+> PP.text "->"
    body = prettyProgLoc m b
prettyExp m (ELetrec defs p)
  = PP.sep [PP.text "letrec", indent defs', PP.text "in", indent (prettyProgLoc m p)]
  where defs' = PP.sep $ map (prettyDef m) defs
prettyExp m (EApply p as)
  = PP.sep [PP.text "apply", indent func, indent args']
  where
    func = prettyProgLoc m p
    args' = args . map (prettyProgLoc m) $ as
prettyExp m (EInterCall m' f as)
  = PP.sep [PP.text "call", indent func, indent args']
  where
    func = PP.cat [prettyProgLoc m m', PP.char ':' <> prettyProgLoc m f]
    args' = args . map (prettyProgLoc m) $ as
prettyExp m (EPrimOpCall o as)
  = PP.sep [PP.text "primop" <+> prettyAtom o, indent args']
  where args' = args . map (prettyProgLoc m) $ as
prettyExp m (ETry e vs1 e1 vs2 e2)
  = PP.sep [ PP.sep [ PP.text "try", indent e' ]
           , PP.text "of" <+> vs1' <+> PP.text "->"
           , indent e1'
           , PP.text "catch" <+> vs2' <+> PP.text "->"
           , indent e2'
           ]
  where
    e' = prettyProgLoc m e
    e1' = prettyProgLoc m e1
    e2' = prettyProgLoc m e2
    vs1' = angles . docseq . map (prettyNameId m) $ vs1
    vs2' = angles . docseq . map (prettyNameId m) $ vs1
prettyExp m (EReceive cs t e)
  = PP.sep [PP.text "receive", clauses, timeout]
  where
    clauses = PP.vcat . map (prettyClause m) $ cs
    timeout = PP.sep [PP.text "after", indent t', PP.text "->", indent e']
    t' = prettyProgLoc m t
    e' = prettyProgLoc m e
prettyExp m (EDo e1 e2)
  = PP.sep [PP.text "do", indent (prettyProgLoc m e1), indent (prettyProgLoc m e2)]

prettyClause :: Module -> Clause -> PP.Doc
prettyClause m (Clause ps g e)
  = PP.sep [patterns, guard, indent $ prettyProgLoc m e]
  where
    patterns = angles . docseq . map (prettyPattern m) $ ps
    guard = PP.text "when" <+> prettyProgLoc m g <+> PP.text "->"

prettyPattern :: Module -> Pattern -> PP.Doc
prettyPattern m (PVar nid) = prettyNameId m nid
prettyPattern m (PLit l) = prettyLiteral l
prettyPattern m (PTuple ps) = tuple . map (prettyPattern m) $ ps
prettyPattern m (PList h t)
  = PP.cat [PP.lbrack <> prettyPattern m h, PP.char '|' <> prettyPattern m t <> PP.rbrack]
prettyPattern m (PBinary bs)
  = PP.text "#{" <> PP.sep (PP.punctuate PP.comma . map prettyBitstring $ bs) <> PP.text "}#"
  where
    bitstrings = PP.sep . PP.punctuate PP.comma . map prettyBitstring $ bs
    prettyBitstring :: (Pattern, [ProgLoc]) -> PP.Doc
    prettyBitstring (p, ps)
      = PP.char '#' <> angles (prettyPattern m p) <> exps
      where exps = args (map (prettyProgLoc m) ps)
prettyPattern m (PAlias nid p)
  = prettyNameId m nid <+> PP.equals <+> prettyPattern m p
