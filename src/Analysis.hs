module Semantics
  ( DataAbs(..)
  , TimeAbs(..)
  , MailAbs(..)
  , State(..)
  , ProcStore(..)
  , ProcState(..)
  , ValueStore(..)
  , KontStore(..)
  , MailboxStore(..)
  , Value(..)
  , Closure(..)
  , Environment(..)
  , Pid(..)
  , ValueAddr(..)
  , KontAddr(..)
  , analyse
  ) where

import Syntax

import Control.Lens.TH (makeLenses)
import Control.Lens (Getter, (.=), (%=), use, at)
import Control.Monad (filterM, mapM)
import Control.Monad.State (State(..), evalState, put, get, modify)
import Data.Map (Map, (!), insert, insertWith)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, fromJust, isJust, catMaybes)
import Data.PSQueue (PSQ, (:->))
import Data.PSQueue as PSQ

-- | Data abstraction. One of the three basic domain abstractions.
class DataAbs d where
  -- | Given a module, a store and a closure, resolves the closure into an
  -- abstract datum.
  resolve :: Module -> ValueStore d t m -> Value d t m -> [d]

-- | Time abstraction. One of the three basic domain abstractions.
class TimeAbs t where
  initTime :: t
  tick :: Progloc -> t -> t
  tick' :: t -> t -> t

-- | Mailbox abstraction. One of the three basic domain abstractions.
class MailAbs m where
  emptyMailbox :: m
  join :: m -> m -> m
  enq :: (DataAbs d) => Module -> ValueStore d t m -> Value d t m -> m -> m
  -- | Takes a list of pattern lists, a mailbox, a module and value store (for
  -- resolving values) and returns a list of possible matches. A possible match
  -- is comprised of the index of the matched pattern list, a substitution
  -- witnessing the match, and the mailbox that results from removing the
  -- matched message.
  mmatch :: (DataAbs d) => Module -> ValueStore d t m -> [[Pattern]] -> m -> [(Int, [(NameId, Value d t m)], m)]

data State d t m
  = State
    { _procStore    :: ProcStore d t m,
    , _mailboxStore :: MailboxStore d t m
    , _valueStore   :: ValueStore d t m
    , _kontStore    :: KontStore d t m
    }
  deriving (Show, Eq, Ord)

makeLenses ''State

type ProcStore d t m
  = Map (Pid d t m) (Set (ProcState d t m))

data ProcState d t m
  = ProcState
    { loc   :: Either ProgLoc (Pid d t m)
    , env   :: Environment d t m
    , kaddr :: KAddr d t m
    , time  :: Time d t m
    }
  deriving (Show, Eq, Ord)

type ValueStore d t m
  = Map (ValueAddr d t m) (Set (Value d t m))

type KontStore d t m
  = Map (KontAddr d t m) (Set (Kont d t m))

type MailboxStore d t m
  = Map (Pid d t m) m

type Value d t m
  = Either (Closure d t m) (Pid d t m)

data Closure d t m
  = Closure Exp (Environment d t m)
  deriving (Show, Eq, Ord)

type Environment d t m
  = Map NameId (ValueAddr d t m)

data Pid d t m
  = Pid ProgLoc t
  deriving (Show, Eq, Ord)

-- | Dummy data that represents the data value of a closure.
data Fun = Fun

data ValueAddr d t m
  = ValueAddr
    { vaPid   :: Pid d t m
    , vaName  :: NameId
    , vaDatum :: Either Fun d
    , vaTime  :: t
    }
  deriving (Show, Eq, Ord)

data KontAddr d t m
  = StopAddr
  | KontAddr
    { kaPid  :: Pid d t m
    , kaLoc  :: ProgLoc
    , kaEnv  :: Environment d t m
    , kaTime :: t
    }
  deriving (Show, Eq, Ord)

-- | A Kont{vals = [], locs = [p_1, ..., p_n] eventually progresses to
-- a Kont{vals = [v_{n-1}, ..., v_1, v_0], locs = []}, at which point we
-- return to the next Kont.
data Kont d t m
  = Stop
  | KLet
    { kNames :: [NameId]
    , kBody  :: ProgLoc
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KCase
    { kClauses :: [Clause]
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KCase'
    { kClauses :: [Clause]
    , kReifs :: [Reification d t m]
    , kEnv'  :: Environment d t m
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KReceive
    { kExpr  :: ProgLoc
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KReceive'
    { kBody  :: ProgLoc
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KApply
    { kExpr  :: ProgLoc  -- Fed into 'tick' on application
    , kVals  :: [Value d t m]
    , kLocs  :: [ProgLoc]
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KInterCall
    { kExpr  :: ProgLoc  -- Fed into 'tick' on application
    , kVals  :: [Value d t m]
    , kLocs  :: [ProgLoc]
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KPrimOpCall
    { kExpr  :: ProgLoc  -- Fed into 'tick' on application
    , kOp    :: Atom
    , kVals  :: [Value d t m]
    , kLocs  :: [ProgLoc]
    , kEnv   :: Environment d t m
    , kNext  :: KontAddr d t m
    }
  | KDo
    { kBody  :: ProgLoc
    , kEnv   :: Ennv d t m
    , kNext  :: KontAddr d t m
    }
  deriving (Show, Eq, Ord)

-- | State for the monad used by the analysis.
data AnalyseState d t m
  = AnalyseState
  { _module :: Module
  -- ^ The module being analysed.
  , _state :: State d t m
  -- ^ One globally widened reachable state.
  , _activeComponent :: (Pid d t m, ProcState d t m)
  -- ^ The component currently under inspection.
  , _vaddrDeps :: Map (ValueAddr d t m) (Set (Pid d t m, ProcState d t m))
  -- ^ Map from value addresss to the active components of states whose
  -- transitions depend on the values stored at that address.
  , _kaddrDeps :: Map (KontAddr d t m) (Set (Pid d t m, ProcState d t m))
  -- ^ Map from kont addresss to the active components of states whose
  -- transitions depend on the konts stored at that address.
  , _mailDeps :: Map (Pid d t m) (Set (Pid d t m, ProcState d t m))
  -- ^ Map from pids to the active components of states whose transitions
  -- depend on the contents of the mailbox of that pid.
  , _worklist :: PSQ (Pid d t m, ProcState d t m) Int
  -- ^ Active components whose transitions need to be (re)computed.
  }

-- _module becomes module :: Lens' (AnalyseState d t m) Module
makeLenses ''AnalyseState

type Analysing d t m = State (AnalyseState d t m)

-- | Calculates the priority with which the successors of the active component
-- of a state need to be recomputed.
priority :: ProcState d t m -> Int
-- TODO: implement this

-- | Intended to be used with getters like @valueDeps.at va@
addWork :: Getter (AnalyseState d t m) (Maybe (Set (Pid d t m, ProcState d t m))) -> Analysing d t m ()
addWork getDeps = do
  deps <- fromMaybe Set.empty <$> use getDeps
  forM deps $ \dep@(_, ps) -> worklist %= PSQ.insert dep (priority ps)

-- | Inserts an element into a set if it exists, or into the empty set if
-- it doesn't.
--
-- >   insert' a Nothing = Just $ Set.insert a Set.empty
-- >   insert' a (Just s) = Just $ Set.insert a s
insert' :: a -> Maybe (Set a) -> Maybe (Set a)
insert' a = Just . Set.insert a . fromMaybe Set.empty

-- | Inserts a message into a mailbox if it exists, or into the empty mailbox if
-- it doesn't.
--
-- >   enq' val Nothing = Just $ enq val emptyMailbox
-- >   enq' val (Just m) = Just $ enq val m
enq' :: Module -> ValueStore d t m -> Value d t m -> Maybe m -> Maybe m
enq' m store val = Just . enq m store val . fromMaybe emptyMailbox

resolveData :: DataAbs d => Value d t m -> Anaylsing d t m [d]
resolveData val = resolve <$> use module <*> use valueStore <*> pure val

resolveProgLoc' :: ProgLoc -> Analysing Exp
resolveProgLoc' p = use module >>= \m -> resolveProgLoc m p

addProcState :: Pid d t m -> ProcState d t m -> Analysing d t m ()
addProcState pid ps = do
  computed <- use $ state.procStore.at pid
  if Set.member ps computed then
    return ()
  else
    worklist %= Set.insert (pid, ps)
  state.procStore.at pid %= insert' ps

addMessage :: MailAbs m => Pid d t m -> Value d t m -> Analysing d t m ()
addMessage pid val = do
  addWork $ mailboxDeps.at pid
  state.mailboxStore.at pid %= enq' val

addValue :: ValueAddr d t m -> Value d t m -> Analysing d t m ()
addValue va val = do
  addWork $ vaddrDeps.at va
  state.valueStore.at va %= insert' val

resolveValueAddr :: ValueAddr d t m -> Analysing d t m (Set (Value d t m))
resolveValueAddr va = do
  comp <- use activeComponent
  vaddrDeps.at va %= insert' comp
  (Just _) <- use $ state.valueStore.at va

addKont :: KontAddr d t m -> Kont d t m -> Analysing d t m ()
addKont ka kont = do
  addWork $ kaddrDeps.at ka
  state.kontStore.at ka %= insert' kont

resolveKontAddr :: KontAddr d t m -> Analysing d t m (Set (Kont d t m))
resolveKontAddr ka = do
  comp <- use activeComponent
  kaddrDeps.at ka %= insert' comp
  (Just konts) <- use $ state.kontStore.at ka
  return konts

getMailbox :: Pid d t m -> Analysing d t m (m)
getMailbox pid = do
  comp <- use activeComponent
  mailboxDeps.at pid %= insert' comp
  (Just m) <- use $ state.mailboxStore.at pid
  return m

setMailbox :: Pid d t m -> m -> Anaylsing d t m ()
setMailbox pid m = do
  addWork $ mailboxDeps.at pid -- This might be unnecessary, since I think we
                               -- only ever setMailbox on strictly smaller stores.
  -- TODO: Ask Emanuele what to do here. I think we join the mailboxes?
  state.mailboxStore.at pid %= Just . join m . fromMaybe emptyMailbox


-- | Tests whether an expression can be reduced. Note that we assume here that
-- all data constructors are applied only to names.
--
-- @
--     'irreducible' = not . 'reducible'
-- @
irreducible :: Exp -> Bool
irreducible EValueList{} = True
irreducible ELit{}       = True
irreducible EName{}      = True
irreducible ETuple{}     = True
irreducible EList{}      = True
irreducible EBinary{}    = True
irreducible EFun{}       = True
irreducible _            = False

-- | Tests whether an expression can be reduced. Note that we assume here that
-- all data constructors are applied only to names.
--
-- @
--     'reducible' = not . 'irreducible'
-- @
reducible = not . irreducible

-- | Compute the set of reachable states.
-- The initial state is
--
-- >   ([i -> apply 'main':'main/0' ()], [i -> empty])
--
-- where @i@ is the id of the initial process and @empty@ is the empty mailbox.
--
-- TODO: add support for multiple modules
analyse :: (MailAbs m) => Module -> [State d t m]
analyse m
  = states $ evalState doWork $
    AnalyseState
      { _module          = m
      , _state           = initState
      , _activeComponent = (initPid, initPS)
      , _vaddrDeps       = Map.empty
      , _kaddrDeps       = Map.empty
      , _mailDeps        = Map.empty
      , _worklist        = PSQ.singleton (initPid, initPS) (priority initPS)
      }
  where
    (ns, vlocs) = unzip (defintions m)
    vas = map (\n -> VAddr initPid n (Left Fun) initTime) ns
    initEnv = Map.fromList (zip ns vas)
    vals = map (\vloc -> Left $ Closure (resolveProgLoc m vloc) initEnv) vlocs
    initState
      = State
        { _procStore    = Map.singleton initPid (Set.singleton initPS)
        , _mailboxStore = Map.singleton initPid emptyMailbox
        , _valueStore   = Map.fromList (zip vas vals)
        , _kontStore    = Map.singleton StopAddr Stop
        }
    initPid = Pid initLoc initTime
    initLoc =  (defintions m) ! fromJust $ exportsAs "main/0" m
    initPS
      = ProcState
        { loc   = Left initLoc
        , env   = initEnv
        , kaddr = StopAddr
        , time  = initTime
        }

doWork :: Anaylsing d t m ()
doWork = do
  wl <- use worklist
  case PSQ.minView wl of
    Nothing -> return ()
    Just (active@(pid, ps) :-> _, wl') -> do
      activeComponent .= active
      worklist .= wl'
      case loc ps of
        Right pid ->
          popKont pid ps (Right pid)
        Left l -> do
          e <- resolveProgLoc' l
          computeTransition pid ps e

-- | Computes the list of possible successors of an active component, given that
-- the active component's 'ProcState' has a 'ProgLoc' that resolves to the
-- argument expression.
--
-- We should have for @evalState (computeTransition pid ps e) st@ that
--
-- >>> (flip evalState) st $ do
-- ...     m <- use module
-- ...     (pid, ps) <- use activeComponent;
-- ...     Just pss <- use $ state.procStore.at pid;
-- ...     return $ e == resolveProgLoc m (loc ps) && Set.member ps pss
-- True
computeTransition :: Pid d t m -> ProcState d t m -> Exp -> Analysing d t m ()
-------------------- reducible states --------------------
computeTransition pid ps ELet { eNames = ns, eHead = hloc, eBody = bloc }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left hloc, kaddr = ka }
  where
    ka = KontAddr pid vloc (env ps) (time ps)
    kont = KLet
      { kNames = ns
      , kBody  = bloc
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps ECase { eHead = hloc, eClauses = clauses }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left hloc, kaddr = ka }
  where
    ka = KontAddr pid vloc (env ps) (time ps)
    kont = KCase
      { kClauses = clauses
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps ELetrec { eDefs = defs, eBody = bloc }
  = do
    let
      (names, vlocs) = unzip defs
      vas = map (\name -> ValueAddr pid name (Left Fun) (time ps)) names
      env' = foldr (uncurry insert) (env ps) (zip names vas)
    ves <- resolveProgLoc' vlocs
    let vals = map (\ve -> Left $ Closure ve env') ves
    mapM (uncurry addValue) (zip vas vals)
    addState pid $ ps { loc = Left bloc, env = env' }
computeTransition pid ps EApply { eFun = floc, eArgs = arglocs }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left floc, kaddr = ka }
  where
    ka = KontAddr pid floc (env ps) (time ps)
    kont = KApply
      { kExpr  = (\Left l -> l) $ loc ps
      , kVals  = []
      , kLocs  = arglocs
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps EInterCall { eModuleName = mnloc, eFunName = fnloc, eArgs = arglocs }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left mnloc, kaddr = ka }
  where
    ka = KontAddr pid mnloc (env ps) (time ps)
    kont = KInterCall
      { kExpr  = (\Left l -> l) $ loc ps
      , kVals  = []
      , kLocs  = fnloc : arglocs
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps EPrimOpCall { eOpName = op, eArgs = (argloc:arglocs) }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left argloc, kaddr = ka }
  where
    ka = KontAddr pid argloc (env ps) (time ps)
    kont = KPrimOpCall
      { kExpr  = (\Left l -> l) $ loc ps
      , kOp    = op
      , kVals  = []
      , kLocs  = arglocs
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps EReceive { eTimeout = tloc }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left tloc, kaddr = ka }
  where
    ka = KontAddr pid tloc (env ps) (time ps)
    kont = KReceive
      { kExpr  = (\Left l -> l) $ loc ps
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
computeTransition pid ps EDo { eHead = hloc, eBody = bloc }
  = do
    addKont ka kont
    addState pid $ ps { loc = Left hloc, kaddr = ka }
  where
    ka = KontAddr pid hloc (env ps) (time ps)
    kont = KDo
      { kBody  = bloc
      , kEnv   = env ps
      , kNext  = kaddr ps
      }
------------------- irreducible states -------------------
computeTransition pid ps e
  = do
    konts <- resolveKontAddr (kaddr ps)
    mapM (popKont pid ps $ Left e) konts

-- | Given a list of lists, returns the list containing all lists constructed
-- by selecting any element from the first list, then any element from the
-- second, and so on.
--
-- TODO: Ask Emanuele if there's a stdlib version of this.
selections :: [[a]] -> [[a]]
selections [] = [[]]
selections (as:ass) = [ (a:bs) | a <- as, bs <- selections ass ]

withAssignments
  :: [NameId]
  -> [Value d t m]
  -> Environment d t m
  -> (Environment d t m -> Analysing d t m a)
  -> Analysing d t m a
withAssignments ns vals env_ act = do
  (pid, ps) <- use activeComponent
  vass <- forM (zip ns vals) (\(n, val) -> do
    ds <- resolveData val
    forM ds (\d -> do
      let va = ValueAddr pid n (Right d) (time ps)
      addValue va val
      return va
    )
  )
  forM (selections vass) $ \vas -> act $ foldr (uncurry insert) env_ (zip ns vas)

enclose :: Environment d t m -> Either Exp (Pid d t m) -> Closure d t m
enclose env (Right pid) = Right pid
enclose env (Left e) = Left $ Closure e env

popKont :: (TimeAbs t, MailAbs m) => Pid d t m -> ProcState d t m -> Either Exp (Pid d t m) -> Kont d t m -> Analysing d t m ()
popKont pid ps _ Stop = return ()
popKont pid ps (Left (EValueList [vn])) kont@KLet { kNames = [n] }
  = do
    let val = (env ps) ! vn
    withAssignments [n] [val] (kEnv kont) $ \env' -> do
      addState ps { loc = Left $ kBody kont, env = env', kaddr = kNext kont }
popKont pid ps (Left (EValueList vns)) kont@KLet { kNames = ns }
  | length vns == length ns = do
    let vals = map (\vn -> (env ps) ! vn) vns
    withAssignments ns vals (kEnv kont) $ \env' -> do
      addState ps { loc = Left $ kBody kont, env = env', kaddr = kNext kont }
  | otherwise = error "cannot unify value lists of different lengths"
popKont pid ps (Left (EValueList _)) KLet { kNames = [n] }
  = error "cannot unify value lists of different lengths"
popKont pid ps e kont@Let { kNames = [n] }
  = do
    let val = enclose (env ps) e
    withAssignments [n] [val] (kEnv kont) $ \env' -> do
      addState ps { loc = Left $ kBody kont, env = env', kaddr = kNext kont }
popKont pid ps _ kont@KLet { kNames = _ }
  = error "can only assign exactly one name in let exp with non-valuelist head"
popKont pid ps e kont@KApply { kLocs = [] }
  = do
    let
      vals = reverse $ enclose (env ps) e : kVals kont
      Left (Closure he henv) = head vals
    forM (options henv he) $ \(fe, fenv) -> do
      let EFun { eNames = ns, eBody = bloc } = fe
      if length ns /= length (tail vals) then
        fail "wrong number of arguments applied to lambda"
      else return ()
      withAssignments ns (tail vals) fenv $ \env' -> do
        let t' = tick (kExpr kont) (time ps)
        addState ps { loc = Left bloc, env = env', kaddr = kNext kont, time = time' }
  where
    -- TODO: Go to an error state if there's a attempted application of something
    -- other than a function here?
    options :: Environment d t m -> Exp -> Analysing d t m (Set (Exp, Environment d t m))
    options env_ e@EFun{} = return Set.singleton (e, env_)
    options env_ EName n = do
        vals <- resolveValueAddr $ env_ ! n
        let
          f (Right _) = return Set.empty
          f (Left (Closure e env_)) = options env_ e
        Set.unions <$> mapM f vals
popKont pid ps e kont@KApply { kLocs = (argloc:arglocs) }
  = do
    addKont ka kont'
    addState pid $ ps { loc = Left argloc, env = kEnv kont, kaddr = ka }
  where
    ka = KontAddr pid argloc (kEnv kont) (time ps)
    kont' = kont
      { kVals  = enclose (env ps) e : kVals kont
      , kLocs  = arglocs
      }
popKont pid ps e kont@KInterCall { kLocs = [] }
  = do
    let
      (mval:fval:vals) = reverse $ enclose (env ps) e : kVals kont
      Left (Closure e0 env0) = mval
      Left (Closure e1 env1) = fval
    forM (options e0 env0) $ \modulename ->
      forM (options e1 env1) $ \functionname ->
        if modulename == Atom "erlang" then
          handlePrimOpCall pid ps kont functionname vals
        else
          -- TODO: Add support for multiple modules here
          error "no support for multiple modules"
  where
    -- TODO: Go to an error state if there's something other than an atom here?
    options :: Environment d t m -> Exp -> Analysing d t m (Set Atom)
    options env_ e@(ELit (LAtom a)) = return Set.singleton a
    options env_ EName n = do
        vals <- resolveValueAddr $ env_ ! n
        let
          f (Right _) = return Set.empty
          f (Left (Closure e env_)) = options env_ e
        Set.unions <$> mapM f vals
popKont pid ps e kont@KInterCall { kLocs = (argloc:arglocs) }
  = do
    addKont ka kont'
    addState pid $ ps { loc = Left argloc, env = kEnv kont, kaddr = ka }
  where
    ka = KontAddr pid argloc (kEnv kont) (time ps)
    kont' = kont
      { kVals  = enclose (env ps) e : kVals kont
      , kLocs  = arglocs
      }
popKont pid ps e kont@KPrimOpCall { kLocs = [], kOp = op }
  = handlePrimOpCall pid ps kont op vals
  where vals = reverse $ enclose (env ps) e : kVals kont
popKont pid ps e kont@KPrimOpCall { kLocs = (argloc:arglocs) }
  = do
    addKont ka kont'
    addState pid $ ps { loc = Left argloc, env = kEnv kont, kaddr = ka }
  where
    ka = KontAddr pid argloc (kEnv kont) (time ps)
    kont' = kont
      { kVals  = enclose (env ps) e : kVals kont
      , kLocs  = arglocs
      }
popKont pid ps (Left timeout) kont@KReceive{}
  = do
    erec <- resolveProgLoc' (kExpr kont)
    let EReceive { eClauses = clauses, eTimeoutBody = tbloc } = erec
    forM_ (options (env ps) timeout) $ \ta -> do
      if ta /= Atom "infinity" then
        addState pid ps { loc = Left bloc, env = kEnv kont, kaddr = kNext kont }
      else return ()
    let
      patss = map cPatterns clauses
      guards = map cWhere clauses
      blocs = map cBody clauses
    store <- use $ state.valueStore
    mailbox <- getMailbox pid -- adds a mailboxDep
    m <- use module
    let matches = mmatch m store patss mailbox
    -- TODO: work out a way to call upon valueaddr dependencies here
    -- maybe make mmatch into an action in the Analysing monad?
    forM matches (\(index, subst, mailbox') ->
      setMailbox mailbox' -- adds work from mailboxDeps
      let
        (ns, vals) <- unzip subst
        gloc = guards !! index
        bloc = blocs !! index
      withAssignments ns vals (kEnv kont) $ \env' -> do
        let
          ka = KontAddr pid guardloc env' (time ps)
          kont' = KReceive'
            { kBody  = bloc
            , kEnv   = env'
            , kNext  = kNext kont
            }
        addKont ka kont'
        addState ps { loc = Left gloc, env = env', kaddr = ka }
      )
    )
  where
    -- TODO: Go to an error state if there's something other than an atom here?
    options :: Environment d t m -> Exp -> Analysing d t m (Set Atom)
    options env_ e@(ELit (LAtom a)) = return Set.singleton a
    options env_ EName n = do
        vals <- resolveValueAddr $ env_ ! n
        let
          f (Right _) = return Set.empty
          f (Left (Closure e env_)) = options env_ e
        Set.unions <$> mapM f vals
-- TODO: KReceive is wrong. We can't attempt to match the next clause until we've
-- evaluated the guard attached to the current clause.
popKont pid ps (Left (ELit (LAtom (Atom "true")))) kont@KReceive'{}
  = addState pid $ ps { loc = Left kBody kont, env = kEnv kont, kaddr = kNext kont }
popKont pid ps _ kont@KReceive'{} = return ()
popKont pid ps _ kont@KDo{}
  = addState pid $ ps { loc = Left kBody kont, env = kEnv kont, kaddr = kNext kont }
popKont pid ps (Left (EValueList ns)) kont@KCase{ kClauses = clauses }
  | all (\c -> length ns == length (cPatterns c)) clauses = do
    valss <- mapM resolveValueAddr . map ((env ps) !) $ ns
    forM_ (selections valss) $ \vals ->
      reifss <- mapM reifications vals
      forM_ (selections reifss) $ \reifs ->
        computeMatch pid ps kont clauses reifs
  | otherwise = error "cannot match patternlist and valuelist of different lengths"
popKont pid ps e kont@KCase { kClauses = clauses }
  | all (\c -> length (cPatterns c) == 1) clauses = do
    let val = enclose (env ps) e
    reifs <- reifications val
    forM_ reifs $ \reif ->
      computeMatch pid ps kont clauses [reif]
  | otherwise = error "cannot match patternlist and valuelist of different lengths"
popKont pid ps e kont@KCase' { kClauses = [] }
  = error "inconceivable!"
popKont pid ps e kont@KCase' { kClauses = (c:cs) } = do
  (canTrue, canFalse) <- isTrue $ enclose (env ps) e
  if canTrue then do
    addState pid $ ps { loc = Left cBody c, env = kEnv' kont, kont = kNext kont }
  else return ()
  if canFalse then do
    computeMatch pid ps kont cs (kReifs kont)
  else return ()
  where
    isTrue :: Value d t m -> Analysing d t m (Bool, Bool)
    isTrue (Left (Closure (ELit (LAtom (Atom "true"))) _)) = return (True, False)
    isTrue (Left (Closure (EName n) venv)) = do
      vals <- resolveValueAddr $ venv ! n
      foldM collapse (False, False) vals
    isTrue _ = return (False, True)

    collapse :: Value d t m -> (Bool, Bool) -> Analysing d t m (Bool, Bool)
    collapse val (canTrue, canFalse)= do
      (canTrue', canFalse') <- isTrue val
      return $ (canTrue || canTrue', canFalse || canFalse')

computeMatch :: Pid d t m -> ProcState d t m -> Kont d t m -> [Clause] -> [Reification d t m] -> Analysing d t m ()
computeMatch pid ps kont [] reifs = error "non-exhaustive patterns"
computeMatch pid ps kont clauses@(c:cs) reifs = do
  let ms = map (uncurry matches) (zip (cPatterns c) reifs)
  if all isJust ms then do
    let (ns, vals) = unzip . concat . catMaybes ms
    withAssignments ns vals (kEnv kont) $ \env' -> do
      let
        gloc = cWhere c
        ka = KontAddr pid gloc env' (time ps)
        kont' = KCase'
          { kClauses = clauses
          , kReifs = reifs
          , kEnv'  = env'
          , kEnv   = kEnv kont
          , kNext  = kNext kont
          }
      addKont ka kont'
      addState ps { loc = Left gloc, env = env', kaddr = ka }
  else do
    computeMatch pid ps kont cs reifs
  where
    matches :: Pattern -> Reification d t m -> Maybe [(NameId, Value d t m)]
    matches (PVar n) (Reification val _) = Just $ [(n, val)]
    matches (PAlias n pat) r@(Reification val _)
      = ((n, val):) <$> matches pat r
    matches (PLit l) (Reification (Left (Closure (ELit l') _) _)
      | l == l'   = Just []
      | otherwise = Nothing
    matches (PTuple pats) (Reification (Left (Closure (ETuple ns) _)) rs)
      | length pats == length n
        = concat <$> mapM (uncurry matches) (zip pats rs)
      | otherwise = Nothing
    matches (PList ph pt) (Reification (Left (Closure (EList _ _))) [rh, rt])
      = (++) <$> matches ph rh <*> matches pt rt
    matches (PBinary _) _ = error "Binary patterns are not supported."
    matches pat (Reification (Left (Closure (EName _) _) [r]) = matches pat r
    matches _ _ = Nothing

data Reification d t m = Reification (Value d t m) [Reification d t m]

-- TODO: This might loop forever
reifications :: Value d t m -> Analysing d t m [Reification d t m]
reifications val@(Right pid) = return $ Reification val []
reifications val@(Left (Closure val venv)) = do
  let
    ns = names val
    vas = map (venv !) ns
  valss <- mapM (Set.elems <$> resolveValueAddr) vas
  forM (selections valss) $ \vals -> do
    reifss <- mapM reifications vals
    mapM (Reification val) $ selections reifss
  where
    names :: Exp -> [NameId]
    names (EValueList ns) = ns
    names (ELit _) = []
    names (EName n) = [n]
    names (ETuple ns) = ns
    names (EList nh nt) = [nh, nt]
    names (EBinary bs) = (\(ns, nss) -> ns ++ concat nss) unzip bs
    names (EFun{}) = []

handlePrimOpCall :: Pid d t m -> ProcState d t m -> Kont d t m -> Atom -> [Value d t m] -> Analysing d t m ()
handlePrimOpCall pid ps kont (Atom "self") []
  = addState pid $ ps { loc = Right pid, kaddr = kNext kont }
handlePrimOpCall pid ps kont (Atom "spawn") [val]
  = forM_ (options val) $ \(fe, fenv) -> do
      if length (eNames fe) == 0 then
        let pid' = newPid pid (kExpr kont) (time ps)
        addProcState pid' $ ps { loc = Left $ eBody fe, kaddr = StopAddr, time = initTime }
        addProcState pid $ ps { loc = Right pid', kEnv = kEnv kont, kaddr = kNext kont }
      else error "arity of function passed to 'spawn'/1 must be 0"
  where
    newPid :: (TimeAbs t) => Pid d t m -> ProgLoc -> t -> Pid d t m
    newPid (Pid l' t') l t = Pid l $ tick' t $ tick l' t'

    options :: Environment d t m -> Exp -> Analysing d t m (Set (Exp, Environment d t m))
    options (Left (Closure e@EFun{} env_)) = return Set.singleton (e, env_)
    options (Left (Closure (EName n) env_)) = do
        vals <- resolveValueAddr $ env_ ! n
        Set.unions <$> mapM options vals
    options _ = error "argument of 'spawn'/1 must be a function"
handlePrimOpCall pid ps kont (Atom "send") [vpid, val]
  = do
    addState pid $ ps { kaddr = kNext kont }
    forM_ (options vpid) $ \pid' -> addMessage pid' val
  where
    options :: Value d t m -> Analysing d t m (Set (Pid d t m))
    options (Right pid) = Set.singleton pid
    options (Left (Closure (EName n) venv)) = do
      vals <- resolveValueAddr $ env_ ! n
      Set.unions <$> mapM options vals
handlePrimOpCall pid ps kont (Atom op) vals
  = error "no support for primitive operation '" ++ op ++ "'/" ++ show (length vals)
  --TODO: Add support for more primitive operations.
